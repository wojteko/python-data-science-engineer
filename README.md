# Python Data Science Engineer - DevTest

The following task is an excellent opportunity to show us your experience, code style and the way you work with data.

## Technical considerations

Your solution should be written using Python 3.x.

We'll be assessing:

- Simple, intention-revealing code, annotated where some assumptions and decisions are made and why.
- Usage of Python idioms and common data science libraries.
- Clear presentation of results of an analysis.
- Professional engineering techniques are shown - proper modularisation of code, usage of object-oriented design, unit testing.
- Usage of linters & code formatters like PyLint and PyCodeStyle.
- Clear documentation of how to use this code, how to install dependencies, what python distribution should be used and so on.
- Usage of Git

We'd be very impressed if you:

* Prepare code compatible with AWS Lambda environment.
* Create a proper CLI/Web (Flask) layer above scripts to provide an easy interface to work with data.
* Use correctly Python idioms where applicable.

## Description

In this task, we present you an example dataset gathered from testers. Your mission is to normalize this data into a sensible format, group it and extract basic knowledge from it.

You can create as many intermediate transformations as you like. By 'transform' we mean a process taking source files and creating a new set of files. Do not check-in these intermediate representations into your solution repository - instead, document how they can be created.

## Dataset

[Download](ds-devtask-events.tar)

## Tasks

1. Get familiar with the data structure in this repository.
2. Right now it is impossible to parse these files using JSON parser. Transform these files into JSON-parseable format.
3. Right now browser events are bundled into a higher-order event type called BrowserRecorderEventsRecorded. Extract these browser events, strip all unnecessary information from them and for every test case id provide browser & device metadata inside every browser event (if not ambiguous). You can find browser & device metadata in a different type of event. Maintain time information inside event payload. The resulting file should be JSON-parseable list of browser events (you can use BrowserEventRecorded "event" type as a name for new events). **Please note that information about browser & device metadata may be located inside another file!**
4. Transform dataset: group events by test case ID and order them into one, linear stream of events.
5. Transform dataset: group events by starting URL and order them into one, linear stream of events.
6. In 4 & 5 you've ordered events using a timestamp. What timestamp have you used and why?
7. Prepare a small report about how many events have been gathered for every starting URL in the dataset and every test case ID in the dataset. You should use some chart library (like matplotlib) to present results. You can also use Jupyter Notebooks. Make the solution generic enough to support recalculation of this report as new data arrives.

## Delivery Solution.

Please share resolution as a private repository on a platform of your choice (GitLab, GitHub, BitBucket). Please do not put the solution to the public repository.

We take 2 days to assess your solution, which will be a significant factor regarding an invitation to the next phase, which is the final interview.

